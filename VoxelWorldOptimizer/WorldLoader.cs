﻿using System.Collections.Generic;
using OpenTK;

namespace VoxelWorldOptimizer
{
    internal static class WorldLoader
    {
        public static IEnumerable<Voxel> FromHeightMap(int[,] heightMap)
        {
            var loadedVoxels = new Dictionary<Vector3d, Voxel>();

            var xLength = heightMap.GetLength(0);
            var yLength = heightMap.GetLength(1);
            for (var x = 0; x < xLength; ++x)
            {
                for (var y = 0; y < yLength; ++y)
                {
                    var height = heightMap[x, y];
                    for (var z = 0; z < height; ++ z)
                    {
                        var voxel = BuildVoxel(x, y, z, loadedVoxels);
                        loadedVoxels.Add(voxel.Location, voxel);
                    }
                }
            }
            return loadedVoxels.Values;
        }

        private static Voxel BuildVoxel(int x, int y, int z, IReadOnlyDictionary<Vector3d, Voxel> loadedVoxels)
        {
            var location = new Vector3d(x, y, z);

            var voxel = new Voxel
            {
                Location = location
            };
            if (x > 0)
            {
                var previousX = x - 1;
                var leftLocation = new Vector3d(previousX, y, z);
                Voxel leftNeighbor;
                if (loadedVoxels.TryGetValue(leftLocation, out leftNeighbor))
                {
                    voxel.LeftNeighbor = leftNeighbor;
                }
            }

            if (y > 0)
            {
                var previousY = y - 1;
                var backLocation = new Vector3d(x, previousY, z);
                Voxel backNeighbor;
                if (loadedVoxels.TryGetValue(backLocation, out backNeighbor))
                {
                    voxel.BackNeighbor = backNeighbor;
                }
            }
            if (z > 0)
            {
                var previousZ = z - 1;
                var bottomLocation = new Vector3d(x, y, previousZ);
                Voxel bottomNeighbor;
                if (loadedVoxels.TryGetValue(bottomLocation, out bottomNeighbor))
                {
                    voxel.BottomNeighbor = bottomNeighbor;
                }
            }
            return voxel;
        }
    }
}
