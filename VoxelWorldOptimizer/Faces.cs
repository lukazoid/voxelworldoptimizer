﻿using System;

namespace VoxelWorldOptimizer
{
    [Flags]
    internal enum Faces
    {
        None = 0,
        Front = 1,
        Back = 2,
        Top = 4,
        Bottom = 8,
        Left = 16,
        Right = 32
    }
}