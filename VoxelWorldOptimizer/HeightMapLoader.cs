﻿using System;
using System.Drawing;
using System.Globalization;

namespace VoxelWorldOptimizer
{
    internal static class HeightMapLoader
    {
        public static int[,] FromFile(string filePath)
        {
            using (var image = new Bitmap(filePath))
            {
                var imageWidth = image.Width;
                var imageHeight = image.Height;
                var heightMap = new int[imageWidth, imageHeight];
                for (var x = 0; x < imageWidth; ++x)
                {
                    for (var y = 0; y < imageHeight; ++y)
                    {
                        var pixelColor = image.GetPixel(x, y);
                        int height;

                        try
                        {
                            height = GetHeight(pixelColor);
                        }
                        catch (ArgumentException ex)
                        {
                            var exceptionMessage = String.Format(CultureInfo.CurrentCulture, "Pixel at X={0}, Y={1} is not a valid height-map color", x, y);
                            throw new ArgumentException(exceptionMessage, "filePath", ex);
                        }

                        heightMap[x, y] = height;
                    }
                }

                return heightMap;
            }
        }

        private static int GetHeight(Color pixelColor)
        {
            if (pixelColor.A == 0)
                return 0;
            if (pixelColor.B == 255)
                return 1;
            if (pixelColor.G == 255)
                return 2;
            if (pixelColor.R == 255)
                return 3;

            throw new ArgumentOutOfRangeException("pixelColor");
        }
    }
}