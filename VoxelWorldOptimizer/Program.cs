﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace VoxelWorldOptimizer
{
    internal class Program
    {
        private static void Main()
        {
            var sw = Stopwatch.StartNew();
            var heightMap = HeightMapLoader.FromFile("HeightMap.png");
            Console.WriteLine("Height-map loaded from file in {0}ms", sw.Elapsed.TotalMilliseconds);

            sw.Restart();
            var world = WorldLoader.FromHeightMap(heightMap).ToList();
            Console.WriteLine("World loaded in {0}ms", sw.Elapsed.TotalMilliseconds);

            sw.Restart();
            DoFullOptimization(world);
            Console.WriteLine("Optimization completed in {0}ms", sw.Elapsed.TotalMilliseconds);

            Console.WriteLine("Press any key to continue . . .");
            Console.ReadKey(true);
        }

        private static void DoFullOptimization(IEnumerable<Voxel> world)
        {
            foreach (var currentVoxel in world)
            {
                var visibleFaces = Faces.None;
                if (currentVoxel.BackNeighbor == null)
                {
                    visibleFaces |= Faces.Back;
                }
                if (currentVoxel.FrontNeighbor == null)
                {
                    visibleFaces |= Faces.Front;
                }
                if (currentVoxel.BottomNeighbor == null)
                {
                    visibleFaces |= Faces.Bottom;
                }
                if (currentVoxel.TopNeighbor == null)
                {
                    visibleFaces |= Faces.Top;
                }
                if (currentVoxel.LeftNeighbor == null)
                {
                    visibleFaces |= Faces.Left;
                }
                if (currentVoxel.RightNeighbor == null)
                {
                    visibleFaces |= Faces.Right;
                }

                currentVoxel.VisibleFaces = visibleFaces;
            }
        }
    }
}