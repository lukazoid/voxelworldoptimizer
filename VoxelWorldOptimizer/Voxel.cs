﻿using System;
using OpenTK;

namespace VoxelWorldOptimizer
{
    internal sealed class Voxel
    {
        private Voxel _backNeighbor;
        private Voxel _bottomNeighbor;
        private Voxel _frontNeighbor;
        private Voxel _leftNeighbor;
        private Voxel _rightNeighbor;
        private Voxel _topNeighbor;

        public Vector3d Location { get; set; }

        public Faces VisibleFaces { get; set; }

        public Voxel LeftNeighbor
        {
            get { return _leftNeighbor; }
            set { SetNeighbor(ref _leftNeighbor, value, (field, newValue) => field.RightNeighbor = newValue); }
        }

        public Voxel RightNeighbor
        {
            get { return _rightNeighbor; }
            set { SetNeighbor(ref _rightNeighbor, value, (field, newValue) => field.LeftNeighbor = newValue); }
        }

        public Voxel TopNeighbor
        {
            get { return _topNeighbor; }
            set { SetNeighbor(ref _topNeighbor, value, (field, newValue) => field.BottomNeighbor = newValue); }
        }

        public Voxel BottomNeighbor
        {
            get { return _bottomNeighbor; }
            set { SetNeighbor(ref _bottomNeighbor, value, (field, newValue) => field.TopNeighbor = newValue); }
        }

        public Voxel BackNeighbor
        {
            get { return _backNeighbor; }
            set { SetNeighbor(ref _backNeighbor, value, (field, newValue) => field.FrontNeighbor = newValue); }
        }

        public Voxel FrontNeighbor
        {
            get { return _frontNeighbor; }
            set { SetNeighbor(ref _frontNeighbor, value, (field, newValue) => field.BackNeighbor = newValue); }
        }

        private void SetNeighbor(ref Voxel neighborField, Voxel newNeighbor, Action<Voxel, Voxel> setOtherNeighbor)
        {
            if (Equals(neighborField, newNeighbor))
                return;

            var oldNeighbor = neighborField;
            neighborField = newNeighbor;
            if (oldNeighbor != null)
            {
                setOtherNeighbor(oldNeighbor, null);
            }
            if (neighborField != null)
            {
                setOtherNeighbor(neighborField, this);
            }
        }
    }
}